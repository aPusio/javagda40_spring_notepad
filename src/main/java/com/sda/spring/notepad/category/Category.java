package com.sda.spring.notepad.category;

import java.util.Set;
import java.util.regex.Pattern;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sda.spring.notepad.note.Note;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Size(min = 2, max = 10, message = "my custom message")
	private String name;
	//	@Pattern(regexp = "[0-9][0-9][a-z]")
	@NotEmpty
	private String color;

	@OneToMany(mappedBy = "category")
	private Set<Note> notes;

	@AssertTrue
	@JsonIgnore
	public boolean isNameValid() {
		char firstChar = name.charAt(0);
		boolean upperCase = Character.isUpperCase(firstChar);
		boolean containsDigits = Pattern
									 .compile("[0-9]")
									 .matcher(name)
									 .find();

		return upperCase && !containsDigits;
	}
}
