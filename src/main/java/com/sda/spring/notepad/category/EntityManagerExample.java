package com.sda.spring.notepad.category;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class EntityManagerExample {
	private final EntityManager entityManager;

	public void sample(){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		//cb.createQuery()
		//...
	}
}
