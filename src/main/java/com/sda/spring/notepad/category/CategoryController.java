package com.sda.spring.notepad.category;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/categories")
public class CategoryController {

	private final CategoryService categoryService;

	@GetMapping("/{id}")
	public ResponseEntity<Category> getById(@PathVariable Long id){
		Optional<Category> category = categoryService.findById(id);

		if(category.isPresent()){
			return ResponseEntity.status(HttpStatus.OK)
				.body(category.get());
		}else{
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.build();
		}
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Category save(@RequestBody @Valid Category category){
		return categoryService.save(category);
	}


}