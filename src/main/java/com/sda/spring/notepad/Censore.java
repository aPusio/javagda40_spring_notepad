package com.sda.spring.notepad;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.sda.spring.notepad.note.Note;

@Component
public class Censore {
	private Map<String, String> blacklist = new HashMap<>();

	@PostConstruct
	public void init(){
		blacklist.put("twoja stara", "twoja szanowna mamusia");
		blacklist.put("wypierdzielaj", "zapraszam do wyjscia");
	}

	public Note censore(Note note){
		String content = note.getContent();

		for (String key : blacklist.keySet()) {
			String censoredContent =
				content.replaceAll(key, blacklist.get(key));
			content = censoredContent;
		}

		note.setContent(content);
		return note;
	}
}
