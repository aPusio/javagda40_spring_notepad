package com.sda.spring.notepad.thymeleaf;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	@GetMapping
	public String getIndex(ModelMap modelMap){
		modelMap.addAttribute("hello", "hello from controller");
		modelMap.addAttribute("title", "controller title");
		modelMap.addAttribute("content", "Lorem ipsum .... Lorem ipsum");
		modelMap.addAttribute("titlearray", List.of("example1", "example2", "example3"));
		return "index";
	}
}
