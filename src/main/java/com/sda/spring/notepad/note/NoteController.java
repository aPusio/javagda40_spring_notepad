package com.sda.spring.notepad.note;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

//@Controller
//@ResponseBody
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class NoteController {

	//api/notes
	private final NoteService noteService;

	@PostMapping("/api/notes")
	public void save(@RequestBody Note note){
		noteService.save(note);
	}

	@DeleteMapping("/api/notes/{id}")
	public void deleteById(@PathVariable Long id){
		noteService.deleteById(id);
	}

	@GetMapping("/api/notes/{id}")
//	public Note getById(@PathVariable("id") Long veryStrangeIdName){
	public ResponseEntity<Note> getById(@PathVariable Long id){
		Note note = null;
		try {
			note = noteService.getById(id);
		} catch (RuntimeException e){
			return ResponseEntity
					   .status(HttpStatus.I_AM_A_TEAPOT)
					   .build();
		}
		return ResponseEntity
			.status(HttpStatus.ACCEPTED)
			.body(note);
	}

//	@RequestMapping(value = "/asdasd", method = RequestMethod.GET)
	@GetMapping("/api/notes")
	public ResponseEntity<List<Note>> getAll(){
		List<Note> all = noteService.getAll();
		return ResponseEntity
				   .status(HttpStatus.OK)
				   .body(all);
	}

	@GetMapping("/api/notes/find")
	public List<Note> getByTitleAndContent(
		@RequestParam("title") String title,
		@RequestParam("content") String content){
		return noteService.getByTitleAndContent(title, content);
	}
}
