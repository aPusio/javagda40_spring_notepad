package com.sda.spring.notepad.note;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringNoteRepository extends CrudRepository<Note, Long> {
	List<Note> findByTitleAndContent(String title, String content);
}
